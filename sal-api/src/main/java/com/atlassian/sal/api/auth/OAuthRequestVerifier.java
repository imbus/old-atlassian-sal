package com.atlassian.sal.api.auth;

/**
 * Used to store whether a request has successfully passed through the OAuth filter in some way.
 * Specifically, this can be used to check if a 2LO request without a user associated with it (e.g. impersonation is
 * disabled) came from a trusted system.
 */
public interface OAuthRequestVerifier {
    boolean isVerified();

    void setVerified(boolean val);

    public void clear();
}
