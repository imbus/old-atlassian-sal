package com.atlassian.sal.api.rdbms;

/**
 * This exception is thrown by {@link com.atlassian.sal.api.rdbms} implementations
 * in cases when interaction with a database fails
 *
 * @since v3.0
 */
public class RdbmsException extends RuntimeException {
    public RdbmsException(final String message) {
        super(message);
    }

    public RdbmsException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public RdbmsException(final Throwable cause) {
        super(cause);
    }
}
