package com.atlassian.sal.api.rdbms;

import com.atlassian.annotations.PublicApi;

/**
 * Provided by the host application for creating {@link com.atlassian.sal.api.rdbms.TransactionalExecutor}.
 * <p/>
 * Note that the TransactionalExecutors created are not considered thread safe.
 *
 * @since 3.0
 */
@PublicApi
public interface TransactionalExecutorFactory {
    /**
     * Create a transactional executor with <code>readOnly</code> set and <code>requiresNew</code> not set
     */
    TransactionalExecutor createExecutor();

    /**
     * Create a transactional executor
     *
     * @param readOnly    initial value for <code>readOnly</code>
     * @param requiresNew initial value for <code>requiresNew</code>
     */
    TransactionalExecutor createExecutor(boolean readOnly, boolean requiresNew);
}
