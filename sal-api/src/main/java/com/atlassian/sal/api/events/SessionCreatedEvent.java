package com.atlassian.sal.api.events;

import javax.annotation.concurrent.Immutable;

/**
 * Represents an event published when a http session is created.  Implementers should fire this event accordingly.
 *
 * @see javax.servlet.http.HttpSessionListener
 */
@Immutable
public class SessionCreatedEvent extends AbstractSessionEvent {

    private SessionCreatedEvent(final String sessionId, final String userName) {
        super(sessionId, userName);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends AbstractSessionEvent.Builder {
        private Builder() {
        }

        public SessionCreatedEvent build() {
            return new SessionCreatedEvent(sessionId, userName);
        }
    }
}

