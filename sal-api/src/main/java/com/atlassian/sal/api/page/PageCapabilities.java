package com.atlassian.sal.api.page;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;

/**
 * Util class for creating empty enum set, parsing string and valueOf function
 */
public abstract class PageCapabilities {

    public static final String SEPARATOR = ",";

    /**
     * @return Empty EnumSet
     */
    public static EnumSet<PageCapability> empty() {
        return EnumSet.noneOf(PageCapability.class);
    }

    /**
     * @param values {@code String} page capability names separated with {@link #SEPARATOR}
     * @return {@link java.util.EnumSet} of {@link com.atlassian.sal.api.page.PageCapability}
     * @throws IllegalArgumentException if any of parsed values is invalid
     */
    public static EnumSet<PageCapability> valueOf(@Nullable String values) {
        if (values == null || values.length() == 0) {
            return empty();
        }

        final Collection<PageCapability> capabilities = Collections2.filter(Lists.transform(Arrays.asList(values.split(SEPARATOR)), new Function<String, PageCapability>() {
            @Override
            public PageCapability apply(final String value) {
                try {
                    return PageCapability.valueOf(value);
                } catch (IllegalArgumentException e) {
                    return null;
                }
            }
        }), Predicates.notNull());

        if (capabilities.isEmpty()) {
            return empty();
        }

        return EnumSet.<PageCapability>copyOf(capabilities);
    }

    /**
     * @param pageCaps enum set
     * @return Page capabilities separated with {@link #SEPARATOR}
     */
    public static String toString(@Nonnull EnumSet<PageCapability> pageCaps) {
        return Joiner.on(SEPARATOR).join(pageCaps.iterator());
    }
}
