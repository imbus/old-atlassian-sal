package com.atlassian.sal.api.rdbms;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Option;

import javax.annotation.Nonnull;

/**
 * Interface allowing the user to execute a callback with a {@link java.sql.Connection}, in the context of a transaction.
 * <p/>
 * Note that participation in an existing transaction i.e. <code>requiresNew</code> is merely a hint to the host
 * application and not binding. Best effort will be attempted.
 * <p/>
 * The following example will query an application's table using JDBC:
 * <pre>
 * {@code
 * final TransactionalExecutor transactionalExecutor = transactionalExecutorFactory
 *         .createExecutor()
 *         .readOnly()
 *         .newTransaction();
 *
 * final String summary = transactionalExecutor.execute(new ConnectionCallback<String>()
 * {
 *     public String execute(final Connection connection)
 *     {
 *         try
 *         {
 *             final String schemaPrefix = transactionalExecutor.getSchemaName().map(s -> s + '.').getOrElse("");
 *
 *             final PreparedStatement preparedStatement = connection.prepareStatement(""
 *                     + "select summary\n"
 *                     + "from " + schemaPrefix + "jiraissue\n"
 *                     + "order by id desc");
 *
 *             final ResultSet resultSet = preparedStatement.executeQuery();
 *
 *             return resultSet.next() ? resultSet.getString("summary") : "no issues";
 *         }
 *         catch (SQLException e)
 *         {
 *             throw new RuntimeException("oh noes!", e);
 *         }
 *     }
 * });
 * }
 * </pre>
 *
 * @see com.atlassian.sal.api.rdbms.TransactionalExecutorFactory
 * @since 3.0
 */
@PublicApi
public interface TransactionalExecutor {
    /**
     * Execute a callback which is supplied a {@link java.sql.Connection}, within a transaction.
     * <p/>
     * After successful execution, the transaction will be committed. If the transaction is within the scope of a larger
     * transaction i.e. <code>requiresNew</code> has been set, it will be scheduled for commit, pending successful
     * completion of the target transaction.
     * <p/>
     * If any exception is thrown by <code>callback</code>, the transaction will be immediately rolled back.
     * <p/>
     * Do not attempt to retain or reuse the Connection outside of the scope of <code>callback</code> or within a
     * different thread. Connection is usually not considered thread safe.
     * <p/>
     * Exceptions thrown from the callback are passed through without wrapping.
     * {@link java.sql.SQLException} occuring in the Executor will be wrapped in {@link com.atlassian.sal.api.rdbms.RdbmsException}.
     * <p/>
     * A {@link java.lang.UnsupportedOperationException} will be thrown on invoking any of the following:
     * <ul>
     * <li>{@link java.sql.Connection#setAutoCommit(boolean)}</li>
     * <li>{@link java.sql.Connection#commit()}</li>
     * <li>{@link java.sql.Connection#close()}</li>
     * <li>{@link java.sql.Connection#rollback()}</li>
     * <li>{@link java.sql.Connection#setReadOnly(boolean)}</li>
     * <li>{@link java.sql.Connection#setReadOnly(boolean)}</li>
     * <li>{@link java.sql.Connection#abort(java.util.concurrent.Executor)}</li>
     * <li>{@link java.sql.Connection#setCatalog(String)}</li>
     * <li>{@link java.sql.Connection#setSchema(String)}</li>
     * <li>{@link java.sql.Connection#setTransactionIsolation(int)}</li>
     * <li>{@link java.sql.Connection#setNetworkTimeout(java.util.concurrent.Executor, int)}</li>
     * </ul>
     *
     * @param callback mandatory, may return {@link java.lang.Void}
     * @return return value of <code>callback</code>
     */
    <A> A execute(@Nonnull ConnectionCallback<A> callback);

    /**
     * Returns the configured schema name (if any), for connections provided during {@link #execute(ConnectionCallback)}.
     *
     * @return schema name, if there is one
     */
    @Nonnull
    Option<String> getSchemaName();

    /**
     * Alter this executor so that the connection is read-only
     *
     * @return this executor
     */
    @Nonnull
    TransactionalExecutor readOnly();

    /**
     * Alter this executor so that the connection is read-write
     *
     * @return this executor
     */
    @Nonnull
    TransactionalExecutor readWrite();

    /**
     * Alter this executor so that it executes in a new transaction, regardless of any existing
     *
     * @return this executor
     */
    @Nonnull
    TransactionalExecutor newTransaction();

    /**
     * Alter this executor so that the connection executes within an existing transaction or creates a new one if one is
     * not present
     *
     * @return this executor
     */
    @Nonnull
    TransactionalExecutor existingTransaction();
}
