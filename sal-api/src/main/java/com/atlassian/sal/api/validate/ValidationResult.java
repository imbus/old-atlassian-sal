package com.atlassian.sal.api.validate;

import com.atlassian.annotations.PublicApi;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import java.util.Collections;

import static com.google.common.base.Preconditions.checkNotNull;

@SuppressWarnings("UnusedDeclaration")
@PublicApi
/**
 * The outcome of a validation check.
 * @since 3.0
 */
public class ValidationResult {
    private static final ValidationResult VALID = new ValidationResult(Collections.<String>emptySet());

    private final ImmutableSet<String> errorMessages;

    private final ImmutableSet<String> warningMessages;

    private ValidationResult(@Nonnull final Iterable<String> errorMessages) {
        this(errorMessages, Collections.<String>emptySet());
    }

    private ValidationResult(@Nonnull final Iterable<String> errorMessages, @Nonnull final Iterable<String> warningMessages) {
        checkNotNull(errorMessages, "errorMessages");
        checkNotNull(warningMessages, "warningMessages");
        this.errorMessages = ImmutableSet.copyOf(errorMessages);
        this.warningMessages = ImmutableSet.copyOf(warningMessages);
    }

    public static ValidationResult valid() {
        return VALID;
    }

    public static ValidationResult withErrorMessages(@Nonnull final Iterable<String> errorMessages) {
        return new ValidationResult(errorMessages);
    }

    public static ValidationResult withWarningMessages(@Nonnull final Iterable<String> warningMessages) {
        return new ValidationResult(Collections.<String>emptySet(), warningMessages);
    }

    public static ValidationResult withErrorAndWarningMessages(@Nonnull final Iterable<String> errorMessages, @Nonnull final Iterable<String> warningMessages) {
        return new ValidationResult(errorMessages, warningMessages);
    }

    /**
     * Return true if the validation passed, false if there were errors. (Note warnings do not cause this method to
     * return false).
     *
     * @return true if the validation passed, false if there were errors.
     * @see #hasWarnings()
     */
    public boolean isValid() {
        return errorMessages.isEmpty();
    }

    /**
     * Return true if the validation added error messages.
     *
     * @return true if the validation failed with error messages.
     */
    public boolean hasErrors() {
        return !errorMessages.isEmpty();
    }

    /**
     * Return true if the validation added warning messages.
     *
     * @return true if the validation failed with warning messages.
     */
    public boolean hasWarnings() {
        return !warningMessages.isEmpty();
    }

    /**
     * Returns error messages.
     * <p/>
     * These should normally be localised to the end user's locale. This will never be null, but may be empty.
     *
     * @return validation error messages, could be empty but not null.
     */
    @Nonnull
    public Iterable<String> getErrorMessages() {
        return errorMessages;
    }

    /**
     * Returns warnings messages.
     * <p/>
     * These should normally be localised to the end user's locale. This will never be null, but may be empty.
     *
     * @return validation warning messages, could be empty but not null.
     */
    @Nonnull
    public Iterable<String> getWarningMessages() {
        return warningMessages;
    }
}
