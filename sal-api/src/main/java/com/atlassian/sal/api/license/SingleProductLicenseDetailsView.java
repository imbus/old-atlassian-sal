package com.atlassian.sal.api.license;

import com.atlassian.annotations.PublicApi;

/**
 * A "view" on top of a license that is in the context of a single given product.
 *
 * Note that some licenses may actually include multiple Products in a single license (eg ELAs - Enterprise License Agreements).
 * However, even if the actually underlying license contains multiple products this view only shows information about
 * a single one of those products.
 *
 * @see com.atlassian.sal.api.license.MultiProductLicenseDetails
 */
@SuppressWarnings("UnusedDeclaration")
@PublicApi
public interface SingleProductLicenseDetailsView extends ProductLicense, BaseLicenseDetails {

}
