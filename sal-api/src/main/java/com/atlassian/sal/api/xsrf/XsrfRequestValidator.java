package com.atlassian.sal.api.xsrf;

import javax.servlet.http.HttpServletRequest;

/**
 * An interface which can be implemented to check if a request
 * contains either a valid csrf token or a
 * valid csrf header {@link com.atlassian.sal.api.xsrf.XsrfHeaderValidator#TOKEN_HEADER}.
 *
 * @since v2.10.18
 */
public interface XsrfRequestValidator {
    /**
     * Returns true iff the given request has a valid csrf token or a
     * valid csrf header.
     *
     * @param request the request to check.
     * @return true iff the given request has a valid csrf token or a
     * valid csrf header.
     */
    public boolean validateRequestPassesXsrfChecks(HttpServletRequest request);
}
