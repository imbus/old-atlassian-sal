package com.atlassian.sal.api;

/**
 * The kind of URL to return from methods that accept this as a parameter.
 */
public enum UrlMode {
    /**
     * Generate a URL that is absolute, using the canonical URL. e.g. http://www.example.com:8080/myapp. Useful for
     * generating links for emails.
     */
    CANONICAL,
    /**
     * Generate a URL that is absolute. Will be derived from the URL of a request in the current scope, or
     * {@link #CANONICAL} if there is no such request. e.g. http://www:8080/myapp.
     */
    ABSOLUTE,
    /**
     * Generate a URL that is relative, containing just the context path. Will be derived from the URL of a request in
     * the current scope, or from {@link #CANONICAL} if there is no such request. e.g. /myapp.
     */
    RELATIVE,
    /**
     * Generate a URL that is relative, containing just the context path. Will be derived from the {@link #CANONICAL}
     * URL. e.g. /myapp.
     */
    RELATIVE_CANONICAL,
    /**
     * Generate a URL that is either {@link #CANONICAL} or {@link #RELATIVE}, based on whether there is a request in
     * the current scope. Useful for macros, web panels and other content that may be rendered in a page or in email.
     */
    AUTO
}
