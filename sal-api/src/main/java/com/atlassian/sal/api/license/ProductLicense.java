package com.atlassian.sal.api.license;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Represents license information for an individual role-based "Product" that lives on a platform.
 */
@SuppressWarnings("UnusedDeclaration")
@PublicApi
public interface ProductLicense {
    static final int UNLIMITED_USER_COUNT = -1;

    /**
     * Returns the canonical product key as defined by the respective source license. Product keys both uniquely
     * identify a product across all Atlassian products, but also define the product's license key namespace.
     * <p>
     * For example, a license containing {@code com.gliffy.integration.jira.active} would return a product key of
     * {@code com.gliffy.integration.jira}, and every other license key relevant to this plugin is expected to be of
     * form:
     * <pre>
     *     com.gliffy.integration.jira.interestingProperty
     * </pre>
     * </p>
     *
     * @return the product key
     */
    @Nonnull
    String getProductKey();

    /**
     * Returns true if this license authorises an unlimited number of users.
     *
     * @return true if this license authorises an unlimited number of users.
     * @see #getNumberOfUsers()
     */
    boolean isUnlimitedNumberOfUsers();

    /**
     * Returns the number of users allowed by the current license.
     *
     * @return the number of user allowed by the license, {@link #UNLIMITED_USER_COUNT} if there is no limit, or the
     * number of users is unlimited.
     * @see #isUnlimitedNumberOfUsers()
     */
    int getNumberOfUsers();

    /**
     * Returns the "official" product name (e.g. "Service Desk") as it is defined in the license, otherwise on a
     * best-effort basis. Typically, product names are expected to be defined by the plugin/product itself, or hardcoded
     * in the host product/platform. The expected use-case for this method is providing a product name for the case
     * where the license is installed but the plugin is not.
     *
     * Note that the product name is almost always a trademark and accordingly should never be internationalised.
     *
     * @return the "official" product name as it appears in the license, or otherwise on a best-effort basis.
     */
    @Nonnull
    String getProductDisplayName();

    /**
     * Returns the license property value for the given property of this product. This is generally expected to be
     * equivalent to the following expression:
     * <pre>
     *      {@link BaseLicenseDetails#getProperty}({@link #getProductKey()} + "." + property)
     * </pre>
     *
     * @param property the license property name, without this product's namespace.
     * @return the license property value, or null if not defined.
     */
    @Nullable
    String getProperty(@Nonnull String property);
}