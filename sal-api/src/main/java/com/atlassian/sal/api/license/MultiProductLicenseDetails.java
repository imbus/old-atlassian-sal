package com.atlassian.sal.api.license;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

/**
 * Provides access to the one or more individual {@link com.atlassian.sal.api.license.ProductLicense products} that may
 * appear in a single license. At the most basic level, licenses are collections of products. Licenses and products
 * across Atlassian vary considerably with respect to the number of licenses that any given product may accept (1-many),
 * but also in terms of the number of products that may be granted within license (also 1-many). Heterogeneous mixes of
 * single-product licenses together with multi-product licenses (e.g. Enterprise Licensing Agreements, ELAs) are also
 * possible within a single product.
 * <p>
 * This interface partitions the products granted by a license into 2 sets:
 * <ul>
 * <li>products that are considered by the host product as sub-products, and/or products that are intrinsic or
 * specific to this host product. For example, in JIRA 7.0, JIRA Service Desk, JIRA Software, and JIRA Core are
 * sub-products.
 * </li>
 * <li>everything else. This typically includes host product and other product add-ons, but also licenses for other
 * top-level Atlassian products, e.g. HipChat, FeCru, etc.</li>
 * </ul>
 * The former are returned by {@link #getProductLicenses()}, the latter by {@link #getEmbeddedLicenses()}. These
 * collections are expected to be mutually exclusive (<code>getEmbeddedLicenses().retainAll(getEmbeddedLicenses())</code>
 * equals the empty set), and the union of these Sets (<code>getEmbeddedLicenses().addAll(getEmbeddedLicenses())</code>)
 * is expected to represent all products found in the source license.
 */
@SuppressWarnings("UnusedDeclaration")
@PublicApi
public interface MultiProductLicenseDetails extends BaseLicenseDetails {
    /**
     * Returns {@link ProductLicense}s for the host product license (if relevant) as well as all products in this
     * license that are direct sub-products of the current host product/platform.
     * <p>
     * By convention, starting with JIRA 7.0, these properties are expected to conform to the
     * naming convention {@code "[host-product-namespace].product.[sub-product-namespace].[property-name]"}, eg:
     * {@code jira.product.jira-software.NumberOfUsers}.
     * </p>
     * <p>
     * Host products that do not have sub-products would return only their host product license.
     * </p>
     *
     * @return all products in this license that are specific to the current host product/platform
     * @see #getEmbeddedLicenses()
     */
    @Nonnull
    Set<ProductLicense> getProductLicenses();

    /**
     * Returns {@link ProductLicense}s for all products in this license that are not for this host product, and/or are
     * not considered to be {@link #getProductLicenses() sub-products} by the host product/platform.
     *
     * @return
     * @see #getProductLicenses()
     */
    @Nonnull
    Set<ProductLicense> getEmbeddedLicenses();

    /**
     * Returns the individual product or embedded license with the given product key.
     *
     * @param productKey the product key.
     * @return the individual product or embedded license with the given product key, or null if that product does not
     * exist in this license.
     */
    @Nullable
    ProductLicense getProductLicense(@Nonnull String productKey);
}
