package com.atlassian.sal.api.license;

import javax.annotation.Nullable;

/**
 * This event gets thrown whenever the host application license changes. This includes additions, updates, and removals.
 *
 * This event will get fired once per license key involved in a change; in other words, if multiple products are licensed
 * from a single newly-added license key, only a single event would be fired.
 *
 * @since v3.0
 */
public interface LicenseChangedEvent {
    /**
     * Returns the previously set license. Will return null when adding a license.
     *
     * @return the previously set license.
     */
    @Nullable
    BaseLicenseDetails getPreviousLicense();

    /**
     * Returns the newly set license. Will return null when removing a license.
     *
     * @return the newly set license.
     */
    @Nullable
    BaseLicenseDetails getNewLicense();
}
