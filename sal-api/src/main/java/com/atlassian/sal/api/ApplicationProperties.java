package com.atlassian.sal.api;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.util.Date;

/**
 * Component for looking up application properties specific to their web interface
 *
 * @since 2.0
 */
@SuppressWarnings("UnusedDeclaration")
public interface ApplicationProperties {
    String PLATFORM_BAMBOO = "bamboo";
    String PLATFORM_BITBUCKET = "bitbucket";
    String PLATFORM_CONFLUENCE = "conf";
    String PLATFORM_CROWD = "crowd";
    String PLATFORM_FECRU = "fisheye";
    String PLATFORM_JIRA = "jira";
    String PLATFORM_STASH = "stash";

    /**
     * Get the base URL of the current application.
     *
     * @return the current application's base URL
     * @deprecated since 2.10. This implementation is application-specific, and unreliable for a cross product plugin.
     * Use {@link #getBaseUrl(UrlMode)} instead.
     */
    @Deprecated
    String getBaseUrl();

    /**
     * Get the base URL of the current application, with respect to the given {@link UrlMode}. This varies as follows:
     * <ul>
     * <li>If {@link UrlMode#CANONICAL} return the configured base URL.</li>
     * <li>If {@link UrlMode#ABSOLUTE} return either the base URL of a request in the current scope, or the
     * configured base URL if there is no such request.</li>
     * <li>If {@link UrlMode#RELATIVE} return either the context path of a request in the current scope, or the
     * configured context path if there is no such request.</li>
     * <li>If {@link UrlMode#RELATIVE_CANONICAL} return the configured context path.</li>
     * <li>If {@link UrlMode#AUTO} return either a relative URL if there is a request in the current scope, or the
     * canonical URL if there is no such request.</li>
     * </ul>
     *
     * @param urlMode the UrlMode to use.
     * @return the current application's base URL.
     */
    @Nonnull
    String getBaseUrl(UrlMode urlMode);

    /**
     * Returns the display name for this application.
     *
     * @return the displayable name of the application
     * @see #getPlatformId()
     */
    @Nonnull
    String getDisplayName();

    /**
     * Returns the exact ID of this application/platform, as defined in HAMS and used for licensing purposes.
     * <p/>
     * Return values include:
     * <ul>
     * <li>{@link #PLATFORM_BAMBOO}</li>
     * <li>{@link #PLATFORM_BITBUCKET}</li>
     * <li>{@link #PLATFORM_CONFLUENCE}</li>
     * <li>{@link #PLATFORM_CROWD}</li>
     * <li>{@link #PLATFORM_FECRU}</li>
     * <li>{@link #PLATFORM_JIRA}</li>
     * <li>{@link #PLATFORM_STASH}</li>
     * </ul>
     * Fisheye / Crucible is a snowflake in that they have two separate licenses with two separate application IDs
     * ("fisheye" and "crucible"). For the purposes of having a single ID for the platform, FeCru will return "fisheye"
     * ({@link #PLATFORM_FECRU}) from this method.
     *
     * @return the ID of this application/platform
     * @see #getDisplayName()
     * @since 3.0
     */
    @Nonnull
    String getPlatformId();

    /**
     * @return the version of the application
     */
    @Nonnull
    String getVersion();

    /**
     * @return the build date of the application
     */
    @Nonnull
    Date getBuildDate();

    /**
     * @return the build number of the application, must be parsable by {@link Long#parseLong(String)}
     */
    @Nonnull
    String getBuildNumber();

    /**
     * @return the home directory of the application or null if none is defined
     */
    @Nullable
    File getHomeDirectory();

    /**
     * Get the value of an application property by its key.
     *
     * @param key The Key of the property to retrieve.
     * @return The value of the property or Null if the property does not exist
     * @deprecated As of SAL 2.7.
     */
    @Deprecated
    String getPropertyValue(String key);
}
