package com.atlassian.sal.api.xsrf;

import javax.servlet.http.HttpServletRequest;

/**
 * Provides an implementation of checking if a request or a header value
 * contains a valid {@link TOKEN_HEADER} header.
 *
 * @since v2.10.12
 */
public final class XsrfHeaderValidator {
    private static final String TOKEN_VALUE = "no-check";

    public static final String TOKEN_HEADER = "X-Atlassian-Token";

    /**
     * Returns true if the given HttpServletRequest contains a valid
     * {@link TOKEN_HEADER} header.
     *
     * @param request the request to check.
     * @return true if the given request contains a valid {@link TOKEN_HEADER}
     * header, otherwise returns false.
     */
    public boolean requestHasValidXsrfHeader(HttpServletRequest request) {
        return isValidHeaderValue(request.getHeader(TOKEN_HEADER));
    }

    /**
     * Returns true if the given header value is valid for the
     * {@link TOKEN_HEADER} header.
     *
     * @param headerValue the value of the {@link TOKEN_HEADER} header.
     * @return true if the given value of the {@link TOKEN_HEADER} header
     * is valid, otherwise returns false.
     */
    public boolean isValidHeaderValue(String headerValue) {
        if (headerValue == null) {
            return false;
        }
        return headerValue.equalsIgnoreCase(TOKEN_VALUE);
    }

}
