package com.atlassian.sal.api.user;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * A JAXB {@link XmlAdapter} which converts to and from {@link UserKey} objects.
 *
 * @since 2.10.4
 */
public class UserKeyXmlAdapter extends XmlAdapter<String, UserKey> {
    @Override
    @Nullable
    public UserKey unmarshal(String stringValue) {
        if (isNotBlank(stringValue)) {
            return new UserKey(stringValue);
        } else {
            return null;
        }
    }

    @Override
    @Nullable
    public String marshal(UserKey userKey) {
        if (userKey != null) {
            return userKey.getStringValue();
        } else {
            return null;
        }
    }
}
