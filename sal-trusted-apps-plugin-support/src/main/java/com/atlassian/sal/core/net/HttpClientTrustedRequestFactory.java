package com.atlassian.sal.core.net;

import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.core.trusted.CertificateFactory;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Does NOT support json/xml oject marshalling. Use the atlassian-rest implementation of
 * {@link com.atlassian.sal.api.net.RequestFactory} instead.
 */
public class HttpClientTrustedRequestFactory extends HttpClientRequestFactory {
    private static final Logger log = LoggerFactory.getLogger(HttpClientTrustedRequestFactory.class);

    private final UserManager userManager;
    private final CertificateFactory certificateFactory;

    public HttpClientTrustedRequestFactory(final UserManager userManager, final CertificateFactory certificateFactory) {
        this.userManager = userManager;
        this.certificateFactory = certificateFactory;
    }

    /* (non-Javadoc)
         * @see com.atlassian.sal.api.net.RequestFactory#createMethod(com.atlassian.sal.api.net.Request.MethodType, java.lang.String)
         */
    public HttpClientTrustedRequest createTrustedRequest(final MethodType methodType, final String url) {
        final CloseableHttpClient httpClient = createHttpClient();
        final HttpClientContext clientContext = createClientContext();
        return new HttpClientTrustedRequest(userManager, certificateFactory, httpClient, clientContext, methodType, url);
    }
}
