package com.atlassian.sal.core.net;

import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.atlassian.sal.api.net.TrustedRequest;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.core.net.auth.TrustedTokenScheme;
import com.atlassian.sal.core.trusted.CertificateFactory;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;

/**
 * HttpClient implementation of Request interface
 */
public class HttpClientTrustedRequest extends HttpClientRequest implements TrustedRequest {

    private final UserManager userManager;
    private final CertificateFactory certificateFactory;

    public HttpClientTrustedRequest(final UserManager userManager, final CertificateFactory certificateFactory, final CloseableHttpClient httpClient, final HttpClientContext httpClientContext, final MethodType initialMethodType, final String initialUrl) {
        super(httpClient, httpClientContext, initialMethodType, initialUrl);
        this.userManager = userManager;
        this.certificateFactory = certificateFactory;
    }

    public HttpClientTrustedRequest addTrustedTokenAuthentication(final String hostname) {
        return addTrustedTokenAuthentication(hostname, userManager.getRemoteUsername());
    }

    public HttpClientTrustedRequest addTrustedTokenAuthentication(final String hostname, final String username) {
        httpClientContext.getCredentialsProvider().setCredentials(
                new AuthScope(hostname, AuthScope.ANY_PORT),
                new UsernamePasswordCredentials(username, null));

        httpClientContext.getAuthCache().put(new HttpHost(hostname),
                new TrustedTokenScheme(certificateFactory.createCertificate(username, this.requestBuilder.getUri().toString())));
        return this;
    }

    @Override
    public void execute(final ResponseHandler responseHandler) throws ResponseException {
        super.execute(responseHandler);
    }

    @Override
    public Object executeAndReturn(final ReturningResponseHandler responseHandler) throws ResponseException {
        return super.executeAndReturn(responseHandler);
    }
}
