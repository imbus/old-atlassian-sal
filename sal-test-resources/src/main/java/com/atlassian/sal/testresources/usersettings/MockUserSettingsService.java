package com.atlassian.sal.testresources.usersettings;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.usersettings.UserSettings;
import com.atlassian.sal.api.usersettings.UserSettingsBuilder;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.atlassian.sal.core.usersettings.DefaultUserSettings;
import com.google.common.base.Function;

import java.util.HashMap;
import java.util.Map;

public class MockUserSettingsService implements UserSettingsService {
    private final Map<String, UserSettings> settingsMap = new HashMap<String, UserSettings>();

    @Override
    public UserSettings getUserSettings(String userName) {
        return settingsMap.containsKey(userName) ? settingsMap.get(userName) : settingsMap.put(userName, DefaultUserSettings.builder().build());
    }

    @Override
    public UserSettings getUserSettings(UserKey user) {
        return getUserSettings(user.getStringValue());
    }

    @Override
    public void updateUserSettings(String userName, Function<UserSettingsBuilder, UserSettings> updateFunction) {
        if (settingsMap.containsKey(userName)) {
            settingsMap.put(userName, updateFunction.apply(DefaultUserSettings.builder(settingsMap.get(userName))));
        } else {
            settingsMap.put(userName, updateFunction.apply(DefaultUserSettings.builder()));
        }
    }

    @Override
    public void updateUserSettings(UserKey user, Function<UserSettingsBuilder, UserSettings> updateFunction) {
        updateUserSettings(user.getStringValue(), updateFunction);
    }
}
