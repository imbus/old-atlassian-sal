package com.atlassian.sal.api.net;

/**
 * Factory to create {@link TrustedRequest}s. Requests are used to make network calls, using Trusted Apps authentication.
 *
 * The rest plugin provides the default implementation for this interface.
 *
 * @param <T> The type of request to create
 * @since 3.0.0
 **/
public interface TrustedRequestFactory<T extends TrustedRequest> extends RequestFactory {
    /**
     * Creates a request of given {@link com.atlassian.sal.api.net.Request.MethodType} to given url.
     * Wraps the request in Trusted Apps authentication.
     *
     * @param methodType
     * @param url
     * @return
     */
    T createTrustedRequest(final Request.MethodType methodType, final String url);
}
