package com.atlassian.sal.core.scheduling;

import com.atlassian.sal.api.scheduling.PluginJob;
import com.google.common.base.Predicate;
import junit.framework.TestCase;

import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

public class ExecutorPluginSchedulerTest extends TestCase {

    private ScheduledExecutorService executor;
    private ExecutorPluginScheduler scheduler;

    protected void setUp() throws Exception {
        executor = Executors.newScheduledThreadPool(1);
        scheduler = new ExecutorPluginScheduler(executor);
    }

    @Override
    public void tearDown() throws Exception {
        executor.shutdown();
    }

    public void testSchedule() throws Exception {
        scheduler.scheduleJob("test1", TestPluginJob.class, Collections.<String, Object>emptyMap(), new Date(), 60000);
        waitForCondition(new Predicate<AtomicInteger>() {
            @Override
            public boolean apply(AtomicInteger input) {
                return input.get() == 1;
            }
        }, 180000);
        scheduler.unscheduleJob("test1");
        assertJobIsNotScheduled("test1");

        scheduler.scheduleJob("test2", TestPluginJob.class, Collections.<String, Object>emptyMap(), new Date(), 500);
        waitForCondition(new Predicate<AtomicInteger>() {
            @Override
            public boolean apply(AtomicInteger input) {
                return input.get() > 1;
            }
        }, 180000);
        scheduler.unscheduleJob("test2");
        assertJobIsNotScheduled("test2");
    }

    public void testUnscheduleInexisting() throws Exception {
        assertJobIsNotScheduled("inexistingjob");
    }

    private void waitForCondition(Predicate<AtomicInteger> condition, int timeoutInMs) throws InterruptedException {
        long start = System.currentTimeMillis();
        while (!condition.apply(TestPluginJob.executionCount) && (System.currentTimeMillis() - start < timeoutInMs)) {
            Thread.sleep(500);
        }
    }

    private void assertJobIsNotScheduled(String jobKey) {
        try {
            scheduler.unscheduleJob(jobKey);
            fail("IllegalArgumentException should have thrown");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("unknown job"));
        }
    }

    public static class TestPluginJob implements PluginJob {
        private static final AtomicInteger executionCount = new AtomicInteger(0);

        @Override
        public void execute(Map<String, Object> jobDataMap) {
            executionCount.incrementAndGet();
        }
    }
}
