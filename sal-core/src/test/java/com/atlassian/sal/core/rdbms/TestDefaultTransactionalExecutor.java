package com.atlassian.sal.core.rdbms;

import com.atlassian.fugue.Option;
import com.atlassian.sal.api.rdbms.RdbmsException;
import org.junit.Test;

import java.sql.SQLException;

import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultTransactionalExecutor extends TestDefaultTransactionalExecutorBase {
    @Test
    public void getSchemaName() {
        when(hostConnectionAccessor.getSchemaName()).thenReturn(Option.option("schema"));

        assertThat(defaultTransactionalExecutor.getSchemaName().get(), is("schema"));
    }

    @Test
    public void changeProperties() {
        defaultTransactionalExecutor.readOnly();
        assertThat(defaultTransactionalExecutor.readOnly, is(true));

        defaultTransactionalExecutor.readWrite();
        assertThat(defaultTransactionalExecutor.readOnly, is(false));

        defaultTransactionalExecutor.newTransaction();
        assertThat(defaultTransactionalExecutor.newTransaction, is(true));

        defaultTransactionalExecutor.existingTransaction();
        assertThat(defaultTransactionalExecutor.newTransaction, is(false));
    }

    @Test
    public void executeCommitSucceeds() throws SQLException {
        assertThat(defaultTransactionalExecutor.executeInternal(connection, callback), is(result));
        assertThat(wrappedConnection.connection, nullValue());

        verify(callback).execute(any(WrappedConnection.class));
        verify(connection).commit();
        verify(connection, never()).rollback();
    }

    @Test
    public void executeRollbackSucceeds() throws SQLException {
        callbackThrows = new CallbackException("epic fail");

        expectedException.expect(CallbackException.class);
        expectedException.expectMessage("epic fail");

        defaultTransactionalExecutor.executeInternal(connection, callback);
        assertThat(wrappedConnection.connection, nullValue());

        verify(callback).execute(any(WrappedConnection.class));
        verify(connection, never()).commit();
        verify(connection).rollback();
    }

    @Test
    public void executeFailsOnAutoCommit() throws SQLException {
        when(connection.getAutoCommit()).thenReturn(true);

        expectedException.expect(IllegalStateException.class);

        defaultTransactionalExecutor.executeInternal(connection, callback);
    }

    @Test
    public void executeAutoCommitFails() throws SQLException {
        SQLException exception = new SQLException("horrible getAutoCommit error");
        doThrow(exception).when(connection).getAutoCommit();

        expectedException.expect(RdbmsException.class);
        expectedException.expectCause(is(exception));

        defaultTransactionalExecutor.executeInternal(connection, callback);
        verify(callback, never()).execute(any(WrappedConnection.class));
    }

    @Test
    public void executeFailsOnAutoCommitGetFail() throws SQLException {
        when(connection.getAutoCommit()).thenThrow(new SQLException("horrible getAutoCommit exception"));

        expectedException.expect(RdbmsException.class);

        defaultTransactionalExecutor.executeInternal(connection, callback);
    }
}
