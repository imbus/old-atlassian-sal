package com.atlassian.sal.core.lifecycle;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.ServiceReference;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.sal.core.lifecycle.LifecycleLog.getPluginKeyFromBundle;
import static com.atlassian.sal.core.lifecycle.LifecycleLog.listPluginKeys;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class TestLifecycleLog extends TestDefaultLifecycleManagerBase {
    @Test
    public void assertPluginKeysEnlistedForStaleServices() {
        final List<ServiceReference<LifecycleAware>> services = registerServices(new String[]{"key1", "key2", "key3"});
        services.add(registerService(null, mock(LifecycleAware.class)));
        assertThat(listPluginKeys(services), is(equalTo("[key1, key2, key3, <stale service reference>]")));
    }

    @Test
    public void assertPluginKeysEnlisted() {
        final List<ServiceReference<LifecycleAware>> services = registerServices(new String[]{"key1", "key2", "key3"});
        assertThat(listPluginKeys(services), is(equalTo("[key1, key2, key3]")));
    }

    @Test
    public void assertBundleKeyRetrieved() {
        assertThat(getPluginKeyFromBundle(mockBundle("key")), is(equalTo("key")));
    }

    @Test
    public void assertBundleKeyRetrievedForStaleService() {
        assertThat(getPluginKeyFromBundle(null), is(equalTo("<stale service reference>")));
    }

    private List<ServiceReference<LifecycleAware>> registerServices(final String pluginKeys[]) {
        final List<ServiceReference<LifecycleAware>> services = new ArrayList<ServiceReference<LifecycleAware>>();

        for (final String k : pluginKeys) {
            services.add(registerService(mockBundle(k), mock(LifecycleAware.class)));
        }
        return services;
    }
}