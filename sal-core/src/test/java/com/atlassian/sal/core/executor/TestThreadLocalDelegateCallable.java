package com.atlassian.sal.core.executor;

import com.atlassian.sal.api.executor.ThreadLocalContextManager;
import com.google.common.base.Throwables;
import junit.framework.TestCase;

import java.util.concurrent.Callable;

public class TestThreadLocalDelegateCallable extends TestCase {
    public void testRun() throws InterruptedException {
        final ThreadLocalContextManager<Object> manager = new StubThreadLocalContextManager();
        Callable<Void> delegate = new Callable<Void>() {
            public Void call() {
                assertNotNull(manager.getThreadLocalContext());
                return null;
            }
        };

        manager.setThreadLocalContext(new Object());
        final Callable callable = new ThreadLocalDelegateCallable<Object, Void>(manager, delegate);
        Thread t = new Thread(new Runnable() {

            public void run() {
                try {
                    callable.call();
                } catch (Exception e) {
                    throw Throwables.propagate(e);
                }
            }
        });
        t.start();
        t.join(10000);
        assertNotNull(manager.getThreadLocalContext());
    }
}