package com.atlassian.sal.core.rdbms;

import com.atlassian.sal.api.rdbms.RdbmsException;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(Parameterized.class)
public class TestDefaultTransactionalExecutorExceptions extends TestDefaultTransactionalExecutorBase {
    @Parameterized.Parameters(name = "{index}: throwing {0}")
    public static Collection data() {
        return Arrays.asList(new Object[][]{
                {new SQLException("horrible commit exception")},
                {new LinkageError("horrible linkage error")},
        });
    }

    @Parameterized.Parameter(0)
    public Throwable exception;

    @Test
    public void executeCommitFails() throws SQLException {
        doThrow(exception).when(connection).commit();

        expectedException.expect(RdbmsException.class);
        expectedException.expectCause(isA(exception.getClass()));

        defaultTransactionalExecutor.executeInternal(connection, callback);
        assertThat(wrappedConnection.connection, nullValue());

        verify(callback).execute(any(WrappedConnection.class));
        verify(connection).commit();
        verify(connection, never()).rollback();
    }

    @Test
    public void executeRollbackFails() throws SQLException {
        callbackThrows = new TestDefaultTransactionalExecutorBase.CallbackException("epic fail");

        doThrow(exception).when(connection).rollback();

        expectedException.expect(CallbackException.class);
        expectedException.expectMessage("epic fail");
        expectedException.expect(new SuppressedMatches(is(exception)));

        defaultTransactionalExecutor.executeInternal(connection, callback);
        assertThat(wrappedConnection.connection, nullValue());

        verify(callback).execute(any(WrappedConnection.class));
        verify(connection, never()).commit();
        verify(connection).rollback();
    }

    private static class SuppressedMatches<T extends Throwable> extends TypeSafeMatcher<T> {
        private Matcher<T> expectedSuppressed;

        public SuppressedMatches(Matcher<T> expectedSuppressed) {
            this.expectedSuppressed = expectedSuppressed;
        }

        @Override
        protected void describeMismatchSafely(final T item, final Description mismatchDescription) {
            mismatchDescription.appendText("was not found in ").appendValue(item.getSuppressed());
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("a suppressed exception matching(");
            expectedSuppressed.describeTo(description);
            description.appendText(")");
        }

        @Override
        public boolean matchesSafely(final Throwable t) {
            for (Throwable s : t.getSuppressed()) {
                if (expectedSuppressed.matches(s)) {
                    return true;
                }
            }

            return false;
        }
    }
}
