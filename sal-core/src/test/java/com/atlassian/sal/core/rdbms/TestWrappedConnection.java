package com.atlassian.sal.core.rdbms;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.concurrent.Executor;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class TestWrappedConnection {
    @Mock
    private Connection mockConnection;

    private WrappedConnection wrappedConnection;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void before() throws Exception {
        wrappedConnection = new WrappedConnection(mockConnection);
    }

    @Test
    public void expire() throws SQLException {
        expectedException.expect(IllegalStateException.class);

        wrappedConnection.expire();

        wrappedConnection.getMetaData();
    }

    /*
     * Banned methods
    */
    @Test
    public void setAutoCommit() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.setAutoCommit(true);
        verifyZeroInteractions(mockConnection);
    }

    @Test
    public void commit() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.commit();
        verifyZeroInteractions(mockConnection);
    }

    @Test
    public void close() throws SQLException {
        wrappedConnection.close();
        verifyZeroInteractions(mockConnection);

        expectedException.expect(IllegalStateException.class);
        wrappedConnection.getMetaData();
    }

    @Test
    public void close_with() throws SQLException {
        WrappedConnection localWrappedConnectionCopy;
        try (WrappedConnection localWrappedConnection = new WrappedConnection(mockConnection)) {
            localWrappedConnectionCopy = localWrappedConnection;
        }
        verifyZeroInteractions(mockConnection);

        expectedException.expect(IllegalStateException.class);
        localWrappedConnectionCopy.getMetaData();
    }

    @Test
    public void rollback() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.rollback();
        verifyZeroInteractions(mockConnection);
    }

    @Test
    public void setReadOnly() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.setReadOnly(true);
        verifyZeroInteractions(mockConnection);
    }

    @Test
    public void abort() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.abort(mock(Executor.class));
        verifyZeroInteractions(mockConnection);
    }

    @Test
    public void setCatalog() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.setCatalog("bazza");
        verifyZeroInteractions(mockConnection);
    }

    @Test
    public void setSchema() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.setSchema("mckenzie");
        verifyZeroInteractions(mockConnection);
    }

    @Test
    public void setTransactionIsolation() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.setTransactionIsolation(Connection.TRANSACTION_NONE);
        verifyZeroInteractions(mockConnection);
    }

    @Test
    public void setNetworkTimeout() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.setNetworkTimeout(mock(Executor.class), 11);
        verifyZeroInteractions(mockConnection);
    }

    @Test
    public void setSavepoint() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.setSavepoint();
        verifyZeroInteractions(mockConnection);
    }

    @Test
    public void setSavepointAlt() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.setSavepoint("bleh");
        verifyZeroInteractions(mockConnection);
    }

    @Test
    public void rollbackSavepoint() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.rollback(mock(Savepoint.class));
        verifyZeroInteractions(mockConnection);
    }

    @Test
    public void releaseSavepoint() throws SQLException {
        expectedException.expect(UnsupportedOperationException.class);
        wrappedConnection.releaseSavepoint(mock(Savepoint.class));
        verifyZeroInteractions(mockConnection);
    }

    /*
     * Delegating methods
     */
    @Test
    public void getMetaData() throws SQLException {
        wrappedConnection.getMetaData();
        verify(mockConnection).getMetaData();
    }
}
