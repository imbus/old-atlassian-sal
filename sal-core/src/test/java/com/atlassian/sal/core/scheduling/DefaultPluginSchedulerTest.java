package com.atlassian.sal.core.scheduling;

import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.status.RunOutcome;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Date;
import java.util.Map;

import static com.atlassian.sal.core.scheduling.DefaultPluginScheduler.JOB_RUNNER_KEY;
import static com.atlassian.sal.core.scheduling.DefaultPluginScheduler.toJobId;
import static com.atlassian.sal.core.util.Assert.notNull;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultPluginSchedulerTest {
    // Used in case the tests are run multi-threaded.  The plugin job instances are created anew for each execution
    // so there is no way to inject them with specific instances.
    private static final ThreadLocal<JobChecker> CALLBACK = new ThreadLocal<JobChecker>() {
        @Override
        protected JobChecker initialValue() {
            return mock(JobChecker.class);
        }
    };

    @Mock
    private SchedulerService schedulerService;
    @Captor
    private ArgumentCaptor<JobRunner> runnerCaptor;

    private DefaultPluginScheduler pluginScheduler;

    @Before
    public void setUp() {
        pluginScheduler = new DefaultPluginScheduler(schedulerService);
        pluginScheduler.afterPropertiesSet();

        verify(schedulerService).registerJobRunner(eq(JOB_RUNNER_KEY), runnerCaptor.capture());
    }

    @After
    public void tearDown() {
        CALLBACK.remove();
    }


    @Test
    public void testUnregisterOnDestroy() {
        pluginScheduler.destroy();

        verify(schedulerService).unregisterJobRunner(eq(JOB_RUNNER_KEY));
    }

    @Test
    public void testSchedule() throws SchedulerServiceException {
        final String jobName = "testjob";
        final JobId jobId = toJobId(jobName);
        final Map<String, Object> jobMap = Collections.<String, Object>singletonMap("a", "b");
        final Date jobStartTime = new Date();
        final int repeatInterval = 60000;

        // Part 1: The actual scheduling
        pluginScheduler.scheduleJob(jobName, TestPluginJob.class, jobMap, jobStartTime, repeatInterval);

        verify(schedulerService).scheduleJob(jobId, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY)
                .withRunMode(RunMode.RUN_LOCALLY)
                .withSchedule(Schedule.forInterval(repeatInterval, jobStartTime)));

        // Part 2: Honouring execute requests
        final JobRunnerRequest request = mock(JobRunnerRequest.class);
        when(request.getJobId()).thenReturn(jobId);

        runnerCaptor.getValue().runJob(request);
        verify(CALLBACK.get()).executedWith(same(jobMap));
    }

    @Test
    public void testUnschedule() throws Exception {
        final String jobName = "testjob";
        final JobId jobId = toJobId(jobName);
        final Map<String, Object> jobMap = Collections.<String, Object>singletonMap("a", "b");
        final Date jobStartTime = new Date();
        final int repeatInterval = 60000;

        pluginScheduler.scheduleJob(jobName, TestPluginJob.class, jobMap, jobStartTime, repeatInterval);

        // Part 1: The actual unscheduling
        pluginScheduler.unscheduleJob(jobName);

        final JobRunnerRequest request = mock(JobRunnerRequest.class);
        when(request.getJobId()).thenReturn(jobId);

        // Part 2: Correct handling of failed execute requests (race conditions)
        final JobRunnerResponse response = notNull(runnerCaptor.getValue().runJob(request), "null response");
        assertThat(response.getRunOutcome(), is(RunOutcome.ABORTED));
        assertThat(response.getMessage(), is("Job descriptor not found"));
        verifyZeroInteractions(CALLBACK.get());
    }

    @Test
    public void testUnscheduleInexisting() {
        final String jobName = "testjob";
        final JobId jobId = toJobId(jobName);

        try {
            pluginScheduler.unscheduleJob(jobName);
            fail("Expected an IllegalArgumentException");
        } catch (IllegalArgumentException iae) {
            assertThat(iae.getMessage(), containsString(jobName));
        }

        // Just in case we're "wrong" and it really is registered.  The scheduler service doesn't mind this.
        verify(schedulerService).unscheduleJob(jobId);
    }

    static class TestPluginJob implements PluginJob {
        @Override
        public void execute(Map<String, Object> jobDataMap) {
            CALLBACK.get().executedWith(jobDataMap);
        }
    }

    static interface JobChecker {
        void executedWith(Map<String, Object> jobDataMap);
    }
}
