package com.atlassian.sal.core.xsrf;

import com.atlassian.sal.api.xsrf.XsrfHeaderValidator;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestXsrfRequestValidator {
    private static final String TOKEN_VALUE = "no-check";
    private XsrfRequestValidator validator;

    @Mock
    private HttpServletRequest mockRequest;
    @Mock
    private XsrfTokenValidator mockValidator;

    @Before
    public void setUp() {
        validator = new XsrfRequestValidatorImpl(mockValidator);
    }

    @Test
    public void testNoTokenAndNoHeader() {
        when(mockValidator.validateFormEncodedToken(mockRequest))
                .thenReturn(false);
        assertFalse(validator.validateRequestPassesXsrfChecks(mockRequest));
    }

    @Test
    public void testValidTokenAndNoHeader() {
        when(mockValidator.validateFormEncodedToken(mockRequest))
                .thenReturn(true);
        assertTrue(validator.validateRequestPassesXsrfChecks(mockRequest));
    }

    @Test
    public void testInvalidTokenValidHeader() {
        when(mockValidator.validateFormEncodedToken(mockRequest))
                .thenReturn(false);
        when(mockRequest.getHeader(XsrfHeaderValidator.TOKEN_HEADER))
                .thenReturn(TOKEN_VALUE);
        assertTrue(validator.validateRequestPassesXsrfChecks(mockRequest));
    }

    @Test
    public void testValidTokenInvalidHeader() {
        when(mockValidator.validateFormEncodedToken(mockRequest))
                .thenReturn(true);
        when(mockRequest.getHeader(XsrfHeaderValidator.TOKEN_HEADER))
                .thenReturn("INVALID_HEADER_VALUE");
        assertTrue(validator.validateRequestPassesXsrfChecks(mockRequest));
    }
}