package com.atlassian.sal.api.xsrf;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestXsrfHeaderValidator {
    private static final String TOKEN_VALUE = "no-check";
    private XsrfHeaderValidator xsrfHeaderValidator;

    private
    @Mock
    HttpServletRequest request;

    @Before
    public void setUp() {
        xsrfHeaderValidator = new XsrfHeaderValidator();
    }

    @Test
    public void testIsValidHeaderValueWithNull() {
        assertEquals(false, xsrfHeaderValidator.isValidHeaderValue(null));
    }

    @Test
    public void testIsValidHeaderValueWithIncorrectValue() {
        assertEquals(false, xsrfHeaderValidator.isValidHeaderValue("a"));
    }

    @Test
    public void testIsValidHeaderValueWithCorrectValue() {
        assertEquals(true, xsrfHeaderValidator.isValidHeaderValue(
                TOKEN_VALUE));
    }

    @Test
    public void testRequestHasValidXsrfHeaderWithValidValue() {
        when(request.getHeader(XsrfHeaderValidator.TOKEN_HEADER)
        ).thenReturn(TOKEN_VALUE);
        assertEquals(true,
                xsrfHeaderValidator.requestHasValidXsrfHeader(request));
    }

    @Test
    public void testRequestHasValidXsrfHeaderWithInvalidValue() {
        when(request.getHeader(XsrfHeaderValidator.TOKEN_HEADER)
        ).thenReturn("a");
        assertEquals(false,
                xsrfHeaderValidator.requestHasValidXsrfHeader(request));
    }

}
