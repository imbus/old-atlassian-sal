package com.atlassian.sal.core.features;

import com.atlassian.sal.api.features.DarkFeatureManager;
import com.google.common.base.Functions;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class SystemDarkFeatureInitializer {
    /**
     * Get the dark features enabled and disabled on startup. Reads from system properties and from a "atlassian-darkfeatures.properties" file.
     * File name can be overridden with darkfeatures.properties.file system property.
     *
     * @return A SystemDarkFeatures containing two sets of strings: one containing enabled dark feature keys and one containing those disabled.
     */
    public static SystemDarkFeatures getSystemStartupDarkFeatures() {
        final String propertiesFile = System.getProperty(DarkFeatureManager.DARKFEATURES_PROPERTIES_FILE_PROPERTY, DarkFeatureManager.DARKFEATURES_PROPERTIES_FILE_PROPERTY_DEFAULT);

        final String disableAllPropertiesFlag = System.getProperty(DarkFeatureManager.DISABLE_ALL_DARKFEATURES_PROPERTY);
        if (Boolean.parseBoolean(disableAllPropertiesFlag)) {
            return SystemDarkFeatures.disableAll();
        }

        final Set<String> enabledPropertyDarkFeatures = getPropertyDarkFeaturesWithValue(true);
        final Set<String> enabledPropertiesFileDarkFeatures = getPropertiesFileDarkFeatures(propertiesFile, true);

        final Set<String> disabledPropertyDarkFeatures = getPropertyDarkFeaturesWithValue(false);
        final Set<String> disabledPropertiesFileDarkFeatures = getPropertiesFileDarkFeatures(propertiesFile, false);

        // System properties override property file
        Set<String> enabled = Sets.union(Sets.difference(enabledPropertiesFileDarkFeatures, disabledPropertyDarkFeatures), enabledPropertyDarkFeatures);
        Set<String> disabled = Sets.union(Sets.difference(disabledPropertiesFileDarkFeatures, enabledPropertyDarkFeatures), disabledPropertyDarkFeatures);

        return SystemDarkFeatures.darkFeatures(enabled, disabled);
    }

    private static Set<String> getPropertiesFileDarkFeatures(String filename, boolean b) {
        final File propertiesFile = new File(filename);

        Properties properties = new Properties();
        try {
            properties.load(new FileReader(propertiesFile));
        } catch (IOException e) {
            return Collections.emptySet();
        }

        return new HashSet<String>(getKeysForBooleanValue(filterOnAndStripKeyPrefix(properties), b));
    }

    private static Set<String> getPropertyDarkFeaturesWithValue(boolean b) {
        return new HashSet<String>(getKeysForBooleanValue(filterOnAndStripKeyPrefix(System.getProperties()), b));
    }

    private static Collection<String> getKeysForBooleanValue(final Map<Object, Object> properties, final Boolean b) {
        return Collections2.transform(Maps.filterValues(properties, new Predicate<Object>() {
            @Override
            public boolean apply(Object input) {
                // don't want to match things with neither true nor false - should be ignored
                return input.toString().toLowerCase().matches(b.toString());
            }
        }).keySet(), Functions.toStringFunction());
    }

    private static Map<Object, Object> filterOnAndStripKeyPrefix(Map<Object, Object> properties) {
        final Set<Map.Entry<Object, Object>> entries = Maps.filterKeys(properties, new Predicate<Object>() {
            @Override
            public boolean apply(Object entry) {
                return entry.toString().startsWith(DarkFeatureManager.ATLASSIAN_DARKFEATURE_PREFIX);
            }
        }).entrySet();

        Map<Object, Object> prefixStrippedEntries = new HashMap<Object, Object>();
        for (Map.Entry<Object, Object> entry : entries) {
            prefixStrippedEntries.put(entry.getKey().toString().substring(DarkFeatureManager.ATLASSIAN_DARKFEATURE_PREFIX.length()), entry.getValue());
        }

        return prefixStrippedEntries;
    }

    public static class SystemDarkFeatures {
        private final Set<String> enabled;
        private final Set<String> disabled;
        private final boolean disableAll;

        private SystemDarkFeatures(Set<String> enabled, Set<String> disabled, boolean disableAll) {
            this.enabled = ImmutableSet.copyOf(enabled);
            this.disabled = ImmutableSet.copyOf(disabled);
            this.disableAll = disableAll;
        }

        public static SystemDarkFeatures disableAll() {
            return new SystemDarkFeatures(Collections.<String>emptySet(), Collections.<String>emptySet(), true);
        }

        public static SystemDarkFeatures darkFeatures(Set<String> enabled, Set<String> disabled) {
            return new SystemDarkFeatures(enabled, disabled, false);
        }

        public Set<String> getEnabled() {
            return enabled;
        }

        public Set<String> getDisabled() {
            return disabled;
        }

        public boolean isDisableAll() {
            return disableAll;
        }
    }
}
