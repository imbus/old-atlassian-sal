package com.atlassian.sal.core.net;

import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.api.net.Request.MethodType;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.MalformedChallengeException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpRequestExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Does NOT support json/xml object marshalling. Use the atlassian-rest implementation of {@link
 * com.atlassian.sal.api.net.RequestFactory} instead.
 */
public class HttpClientRequestFactory implements NonMarshallingRequestFactory<HttpClientRequest<?, ?>> {
    private static final Logger log = LoggerFactory.getLogger(HttpClientRequestFactory.class);

    private final ProxyConfig proxyConfig;

    public HttpClientRequestFactory() {
        this(new SystemPropertiesProxyConfig());
    }

    public HttpClientRequestFactory(final ProxyConfig proxyConfig) {
        this.proxyConfig = proxyConfig;
    }

    /* (non-Javadoc)
     * @see com.atlassian.sal.api.net.RequestFactory#createMethod(com.atlassian.sal.api.net.Request.MethodType, java.lang.String)
     */
    public HttpClientRequest createRequest(final MethodType methodType, final String url) {
        log.debug("Creating HttpClientRequest with proxy config:", proxyConfig);

        final CloseableHttpClient httpClient = createHttpClient();
        final HttpClientContext clientContext = createClientContext();
        return new HttpClientRequest(httpClient, clientContext, methodType, url);
    }

    protected CloseableHttpClient createHttpClient() {
        return HttpClients.custom()
                .useSystemProperties()
                .setRoutePlanner(getRoutePlanner())
                .setRequestExecutor(getRequestExecutor())
                .setConnectionManager(getConnectionManager())
                .build();
    }

    protected HttpClientContext createClientContext() {
        final HttpClientContext httpClientContext = HttpClientContext.create();
        final AuthCache authCache = new AllPortsAuthCache();
        final CredentialsProvider basicCredentialsProvider = new BasicCredentialsProvider();

        if (proxyConfig.requiresAuthentication()) {
            HttpHost proxyHost = new HttpHost(proxyConfig.getHost(), proxyConfig.getPort());
            final AuthScope proxyAuthScope = new AuthScope(proxyHost);
            final Credentials proxyCredentials = new UsernamePasswordCredentials(proxyConfig.getUser(),
                    proxyConfig.getPassword());
            basicCredentialsProvider.setCredentials(proxyAuthScope, proxyCredentials);

            // This ensures that proxy authentication is preemptive.
            BasicScheme proxyScheme = new BasicScheme();
            try {
                proxyScheme.processChallenge(
                        new BasicHeader(HttpHeaders.PROXY_AUTHENTICATE, "Basic "));
            } catch (MalformedChallengeException e) {
                throw new IllegalStateException(e);
            }
            authCache.put(proxyHost, proxyScheme);
        }

        httpClientContext.setCredentialsProvider(basicCredentialsProvider);
        httpClientContext.setAuthCache(authCache);
        return httpClientContext;
    }

    public boolean supportsHeader() {
        return true;
    }

    protected HttpRoutePlanner getRoutePlanner() {
        return proxyConfig.isSet() ? new ProxyRoutePlanner(proxyConfig) : null;
    }

    /**
     * We can override the to override the request execution behaviour. This is useful for testing, but potentially
     * also useful in other scenarious.
     *
     * @return HttpRequestExecutor
     */
    protected HttpRequestExecutor getRequestExecutor() {
        return null;
    }

    protected HttpClientConnectionManager getConnectionManager() {
        return null;
    }

}
