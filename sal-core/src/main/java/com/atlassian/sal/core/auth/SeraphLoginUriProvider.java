package com.atlassian.sal.core.auth;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.page.PageCapabilities;
import com.atlassian.sal.api.page.PageCapability;
import com.atlassian.sal.api.user.UserRole;
import com.atlassian.seraph.config.SecurityConfigFactory;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.EnumSet;

/**
 * @since 2.0.2
 */
public class SeraphLoginUriProvider implements LoginUriProvider {
    private final ApplicationProperties applicationProperties;

    public SeraphLoginUriProvider(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public URI getLoginUri(final URI returnUri) {
        return getLoginUri(returnUri, PageCapabilities.empty());
    }

    @Override
    public URI getLoginUri(final URI returnUri, final EnumSet<PageCapability> pageCaps) {
        return getLoginUriForRole(returnUri, UserRole.USER, pageCaps);
    }

    @Override
    public URI getLoginUriForRole(final URI returnUri, final UserRole role) {
        return getLoginUriForRole(returnUri, role, PageCapabilities.empty());
    }

    @Override
    public URI getLoginUriForRole(final URI returnUri, final UserRole role, final EnumSet<PageCapability> pageCaps) {
        final String loginURL = SecurityConfigFactory.getInstance().getLoginURL(true, true);
        try {
            final String newUrl = loginURL
                    .replace("${originalurl}", URLEncoder.encode(returnUri.toString(), "UTF-8"))
                    .replace("${userRole}", role.toString())
                    .replace("${pageCaps}", PageCapabilities.toString(pageCaps));
            return new URI(applicationProperties.getBaseUrl(UrlMode.AUTO) + newUrl);
        } catch (final URISyntaxException e) {
            throw new RuntimeException("Error getting login uri. LoginUrl = " + loginURL + ", ReturnUri = " + returnUri, e);
        } catch (final UnsupportedEncodingException e) {
            throw new RuntimeException("Error getting login uri. LoginUrl = " + loginURL + ", ReturnUri = " + returnUri, e);
        }
    }
}
