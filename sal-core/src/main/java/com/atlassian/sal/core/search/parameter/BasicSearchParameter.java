package com.atlassian.sal.core.search.parameter;

import com.atlassian.sal.api.search.parameter.SearchParameter;
import org.apache.commons.lang.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Basic name value pair search parameter.
 */
public class BasicSearchParameter implements SearchParameter {
    private String name;
    private String value;

    public BasicSearchParameter(String queryString) {
        initFromQueryString(queryString);
    }

    public BasicSearchParameter(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String buildQueryString() {
        final String encodedName;
        final String encodedValue;
        try {
            encodedName = URLEncoder.encode(name, "UTF-8");
            encodedValue = URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("unable to encode query parameters in UTF-8", e);
        }
        return encodedName + "=" + encodedValue;
    }

    private void initFromQueryString(String queryString) {
        if (StringUtils.isEmpty(queryString) || queryString.indexOf("=") == -1) {
            throw new IllegalArgumentException("QueryString '" + queryString + "' does not appear to be a valid query string");
        }

        try {
            final String[] encodedQueryKeyValuePair = queryString.split("=");
            this.name = URLDecoder.decode(encodedQueryKeyValuePair[0], "UTF-8");
            this.value = URLDecoder.decode(encodedQueryKeyValuePair[1], "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BasicSearchParameter that = (BasicSearchParameter) o;

        if (!name.equals(that.name)) {
            return false;
        }
        if (!value.equals(that.value)) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        int result;
        result = name.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }
}
