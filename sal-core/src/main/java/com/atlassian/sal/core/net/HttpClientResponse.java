package com.atlassian.sal.core.net;

import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class HttpClientResponse implements Response {

    private final int statusCode;
    private final String statusText;
    private final byte[] body;
    private final Charset bodyCharset;
    private final Map<String, String> headers;

    public HttpClientResponse(final CloseableHttpResponse response) throws IOException {
        statusCode = response.getStatusLine().getStatusCode();
        statusText = response.getStatusLine().getReasonPhrase();
        body = (response.getEntity() != null) ? EntityUtils.toByteArray(response.getEntity()) : new byte[0];
        bodyCharset = ContentType.getOrDefault(response.getEntity()).getCharset();
        final Map<String, String> extractedHeaders = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        for (Header header : response.getAllHeaders()) {
            extractedHeaders.put(header.getName(), header.getValue());
        }
        headers = Collections.unmodifiableMap(extractedHeaders);
    }

    @Override
    public int getStatusCode() {
        return statusCode;
    }

    @Override
    public String getStatusText() {
        return statusText;
    }

    @Override
    public boolean isSuccessful() {
        return statusCode >= 200 && statusCode < 400;
    }

    @Override
    public String getHeader(final String name) {
        return headers.get(name);
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public String getResponseBodyAsString() throws ResponseException {
        Charset decodeCharset = (bodyCharset != null) ? bodyCharset : Charset.defaultCharset();
        return new String(body, decodeCharset);
    }

    @Override
    public InputStream getResponseBodyAsStream() throws ResponseException {
        return new ByteArrayInputStream(body);
    }

    @Override
    public <T> T getEntity(final Class<T> entityClass) throws ResponseException {
        throw new UnsupportedOperationException("Not implemented");
    }

}
