package com.atlassian.sal.core.auth;

import com.atlassian.sal.api.auth.OAuthRequestVerifier;
import com.atlassian.sal.api.auth.OAuthRequestVerifierFactory;

import javax.servlet.ServletRequest;

public class OAuthRequestVerifierFactoryImpl implements OAuthRequestVerifierFactory {
    private static final OAuthRequestVerifier instance = new OAuthRequestVerifierImpl();

    /**
     * @param request ignored for default ThreadLocal implementation
     */
    public OAuthRequestVerifier getInstance(ServletRequest request) {
        return instance;
    }
}
