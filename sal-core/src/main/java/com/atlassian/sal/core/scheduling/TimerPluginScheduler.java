package com.atlassian.sal.core.scheduling;

import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Plugin scheduler that uses java.util.Timer
 */
public class TimerPluginScheduler implements PluginScheduler, DisposableBean {

    private final Map<String, Timer> tasks;
    private final boolean useDaemons;

    public TimerPluginScheduler() {
        this(Collections.synchronizedMap(new HashMap<String, Timer>()), false);
    }

    protected TimerPluginScheduler(Map<String, Timer> tasks, boolean useDaemons) {
        this.tasks = tasks;
        this.useDaemons = useDaemons;
    }

    public synchronized void scheduleJob(final String name, final Class<? extends PluginJob> job, final Map<String, Object> jobDataMap, final Date startTime, final long repeatInterval) {
        // Use one timer per task, this will allow us to remove them if that functionality is wanted in future
        Timer timer = tasks.get(name);
        PluginTimerTask task;
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer("PluginSchedulerTask-" + name, useDaemons);
        tasks.put(name, timer);
        task = new PluginTimerTask();
        task.setJobClass(job);
        task.setJobDataMap(jobDataMap);
        timer.scheduleAtFixedRate(task, startTime, repeatInterval);
    }

    @Override
    public void destroy() throws Exception {
        for (Timer timer : tasks.values()) {
            timer.cancel();
        }
    }

    /**
     * TimerTask that executes a PluginJob
     */
    private static class PluginTimerTask extends TimerTask {
        private Class<? extends PluginJob> jobClass;
        private Map<String, Object> jobDataMap;
        private static final Logger log = LoggerFactory.getLogger(PluginTimerTask.class);

        @Override
        public void run() {
            PluginJob job;
            try {
                job = jobClass.newInstance();
            } catch (final InstantiationException ie) {
                log.error("Error instantiating job", ie);
                return;
            } catch (final IllegalAccessException iae) {
                log.error("Cannot access job class", iae);
                return;
            }
            job.execute(jobDataMap);
        }

        public Class<? extends PluginJob> getJobClass() {
            return jobClass;
        }

        public void setJobClass(final Class<? extends PluginJob> jobClass) {
            this.jobClass = jobClass;
        }

        public Map<String, Object> getJobDataMap() {
            return jobDataMap;
        }

        public void setJobDataMap(final Map<String, Object> jobDataMap) {
            this.jobDataMap = jobDataMap;
        }
    }

    public void unscheduleJob(final String name) {
        final Timer timer = tasks.remove(name);
        if (timer != null) {
            timer.cancel();
        } else {
            throw new IllegalArgumentException("Attempted to unschedule unknown job: " + name);
        }
    }
}
