package com.atlassian.sal.core.rdbms;

import com.atlassian.sal.api.rdbms.TransactionalExecutor;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;
import com.atlassian.sal.spi.HostConnectionAccessor;

/**
 * Default implementation.
 * <p/>
 * Host should instantiate and export this.
 *
 * @since 3.0
 */
public class DefaultTransactionalExecutorFactory implements TransactionalExecutorFactory {
    private final HostConnectionAccessor hostConnectionAccessor;

    public DefaultTransactionalExecutorFactory(final HostConnectionAccessor hostConnectionAccessor) {
        this.hostConnectionAccessor = hostConnectionAccessor;
    }

    @Override
    public TransactionalExecutor createExecutor() {
        return new DefaultTransactionalExecutor(hostConnectionAccessor, true, false);
    }

    @Override
    public TransactionalExecutor createExecutor(boolean readOnly, boolean newTransaction) {
        return new DefaultTransactionalExecutor(hostConnectionAccessor, readOnly, newTransaction);
    }
}
