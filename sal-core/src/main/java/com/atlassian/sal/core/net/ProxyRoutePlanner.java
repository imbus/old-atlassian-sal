package com.atlassian.sal.core.net;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.impl.conn.DefaultRoutePlanner;
import org.apache.http.protocol.HttpContext;

public class ProxyRoutePlanner extends DefaultRoutePlanner {
    private static final String NON_PROXY_WILDCARD = "*";

    private final HttpHost proxy;
    private final String[] nonProxyHosts;

    public ProxyRoutePlanner(final ProxyConfig proxyConfig) {
        super(null);
        this.proxy = new HttpHost(proxyConfig.getHost(), proxyConfig.getPort());
        this.nonProxyHosts = proxyConfig.getNonProxyHosts();
    }

    @Override
    protected HttpHost determineProxy(final HttpHost target, final HttpRequest request, final HttpContext context)
            throws HttpException {
        return shouldBeProxied(target.getHostName()) ? proxy : null;
    }

    private boolean shouldBeProxied(final String host) {
        if (StringUtils.isBlank(host)) {
            return false;
        }

        for (final String nonProxyHost : nonProxyHosts) {
            if (nonProxyHost.startsWith(NON_PROXY_WILDCARD)) {
                if (host.endsWith(nonProxyHost.substring(1))) {
                    return false;
                }
            } else if (host.equals(nonProxyHost)) {
                return false;
            }
        }

        return true;
    }

}
