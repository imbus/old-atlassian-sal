package com.atlassian.sal.spring.connection;

import com.atlassian.fugue.Option;
import com.atlassian.sal.api.rdbms.ConnectionCallback;
import com.atlassian.sal.spi.HostConnectionAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Default implementation for spring environments.
 * <p/>
 * Host should instantiate and export this.
 * <p/>
 * Host must implement ConnectionProvider.
 *
 * @since 3.0
 */
public class SpringHostConnectionAccessor implements HostConnectionAccessor {
    private static final Logger log = LoggerFactory.getLogger(SpringHostConnectionAccessor.class);

    private final ConnectionProvider connectionProvider;

    private final PlatformTransactionManager transactionManager;

    public SpringHostConnectionAccessor(@Nonnull final ConnectionProvider connectionProvider, @Nonnull final PlatformTransactionManager transactionManager) {
        this.connectionProvider = connectionProvider;
        this.transactionManager = transactionManager;
    }

    @Override
    public <A> A execute(final boolean readOnly, final boolean newTransaction, @Nonnull final ConnectionCallback<A> callback) {
        // create a transaction with the properties requested
        final DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
        transactionDefinition.setName("SALSpringTx");
        transactionDefinition.setReadOnly(readOnly);
        transactionDefinition.setPropagationBehavior(newTransaction ? TransactionDefinition.PROPAGATION_REQUIRES_NEW : TransactionDefinition.PROPAGATION_REQUIRED);

        // transaction template
        final TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager, transactionDefinition);

        // execute the callback within the transaction
        //noinspection unchecked
        return (A) transactionTemplate.execute(new TransactionCallback() {
            @Override
            public Object doInTransaction(final TransactionStatus status) {

                // retrieve the connection
                final Connection connection = connectionProvider.getConnection();

                // execute the user's callback
                return callback.execute(connection);
            }
        });
    }

    @Nonnull
    @Override
    public Option<String> getSchemaName() {
        return connectionProvider.getSchemaName();
    }

    /**
     * Host specific implementation.
     */
    public interface ConnectionProvider {
        /**
         * Supply a connection to be used within {@link org.springframework.transaction.support.TransactionTemplate#execute(org.springframework.transaction.support.TransactionCallback)}
         * <p/>
         * Note that this should be a "regular" connection, not a {@link com.atlassian.sal.core.rdbms.WrappedConnection}.
         * <p/>
         * Example implementations might be:
         * <code>return dataSource.getConnection();</code>
         * or
         * <code>return sessionFactory.getSession().connection();</code>
         */
        @Nonnull
        Connection getConnection();

        /**
         * Returns the configured schema name (if any), for connections provided by {@link #getConnection()}.
         * <p/>
         * The host must ensure that the provided connection has <code>autoCommit=false</code>.
         *
         * @return schema name, if there is one
         */
        @Nonnull
        Option<String> getSchemaName();
    }
}
